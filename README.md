sublove2d
=========

A Sublime Text 2 plugin for Love2D projects.

SETUP
-----

In your user settings, set the location of both the love2d main executable, and, if you'd like, the location of a dll jit based exe (for use with Decoda)

ex:

    {
    	"love2dexe": "c:/Program Files (x86)/love/love.exe",
    	"love2djitdll": "c:/dev/love2d/dynjit/love.exe"
    }

USAGE
-----

Project -> New Love2d Project. 
Save this new project file whereever your main.lua is (or will be).

Open the project. You will see all your lua files in the sidebar. 

Tools -> Build System -> LOVE

F7

