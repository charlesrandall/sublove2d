import sublime, sublime_plugin, json

class TestBrokenDollarCommand(sublime_plugin.WindowCommand):
	def run(self):
		v = self.window.new_file()

class NewLove2dProjectCommand(sublime_plugin.WindowCommand):
	def run(self):
		v = self.window.new_file()

		v.set_syntax_file('Packages/JavaScript/JSON.tmLanguage')
		v.set_name('untitled.sublime-project')

		proj = {
			"folders" : [ { "path" : "./"} ],
			"build_systems" : [ 
								{ "name" : "LOVE", 
								  "cmd" : [ "love2d", "--console", "$project_path" ],
								  "target" : "run_love2d"
								},
								{
								 "name" : "LOVE JIT Dll",
								 "cmd" : [ "love2d_jit_dll", "$project_path"],
								 "target" : "run_love2d"
								}
							  ]

		}

		template = json.dumps(proj, indent=4)
		edit = v.begin_edit()
		v.insert(edit, 0, template)
		v.end_edit(edit)

#    "love2dexe": "c:/Program Files (x86)/love/love.exe",
#    "love2djitdll": "c:/dev/love2d/dynjit/love.exe"

class RunLove2dCommand(sublime_plugin.WindowCommand):
	def run(self, cmd = []):
		s = sublime.load_settings("Preferences.sublime-settings")

		if cmd[0] == "love2d":
			cmd[0] = s.get("love2dexe")
		elif cmd[0] == "love2d_jit_dll":
			cmd[0] = s.get("love2djitdll")

		if cmd[0] == None:
			print("""
Love2D settings not found!
Please add settings to Preferences -> Settings - User
Like these, but with your own correct paths:
"love2dexe": "c:/Program Files (x86)/love/love.exe",
"love2djitdll": "c:/dev/love2d/dynjit/love.exe" """)
			self.window.run_command("show_panel", {"panel":"console"})
			return

		self.window.run_command("exec", { "cmd" :cmd })
		#self.view.insert(edit, 0, "Hello, World!")

